package org.communiquons.pourcentage;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.communiquons.pourcentage.dialogs.AboutDialog;
import org.communiquons.pourcentage.fragments.SimplePercentageFragment;
import org.communiquons.pourcentage.fragments.VariationFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#3C3F41"));
            getSupportActionBar().setBackgroundDrawable(colorDrawable);
        }

        // Force dark mode
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        onNavigationItemSelected(
                bottomNavigationView.getMenu().findItem(R.id.fragment_simple_percentage));

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.fragment_simple_percentage:
                showFragment(new SimplePercentageFragment());
                break;

            case R.id.fragment_variation:
                showFragment(new VariationFragment());
                break;

            default:
                return false;
        }

        setTitle(item.getTitle());

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_about) {
            AboutDialog.show(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Show a fragment
     *
     * @param fragment The fragment to show
     */
    private void showFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_target, fragment);
        transaction.commit();
    }
}
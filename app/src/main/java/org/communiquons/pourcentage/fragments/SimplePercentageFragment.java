package org.communiquons.pourcentage.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.communiquons.pourcentage.R;
import org.communiquons.pourcentage.views.ValueInputView;
import org.communiquons.pourcentage.views.ValueInputViewInterface;

public class SimplePercentageFragment extends Fragment implements ValueInputViewInterface.OnValueSelectedListener, ValueInputViewInterface.OnValueChangedListener {

    private ValueInputViewInterface percentView;
    private ValueInputViewInterface totalView;
    private ValueInputViewInterface partView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_simple_percentage, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        percentView = view.findViewById(R.id.valuePercent);
        percentView.setLabel(R.string.label_percent);
        percentView.setSelected(true);
        percentView.setOnValueChangedListener(this);
        percentView.setOnValueSelectedListener(this);

        totalView = view.findViewById(R.id.valueTotal);
        totalView.setLabel(R.string.label_of);
        totalView.setOnValueChangedListener(this);
        totalView.setOnValueSelectedListener(this);

        partView = view.findViewById(R.id.valuePart);
        partView.setLabel(R.string.equal);
        partView.setOnValueChangedListener(this);
        partView.setOnValueSelectedListener(this);

        refreshUI();
    }

    @Override
    public void onSelected(ValueInputView v) {
        percentView.setSelected(v == percentView);
        totalView.setSelected(v == totalView);
        partView.setSelected(v == partView);
        refreshUI();
    }

    @Override
    public void onChanged(double value, ValueInputView v) {
        refreshUI();
    }

    private void refreshUI() {
        // Find percentage
        if (percentView.isSelected()) {
            if (totalView.isEmpty() || partView.isEmpty()) {
                percentView.setValue(null);
            } else if (totalView.getValue() == 0) {
                percentView.setValue(0d);
            } else {
                double value = (partView.getValue() * 100) / totalView.getValue();
                percentView.setValue(value);
            }
        }


        // Find the total
        else if (totalView.isSelected()) {

            if (percentView.isEmpty() || partView.isEmpty()) {
                totalView.setValue(null);
            } else if (percentView.getValue() == 0) {
                totalView.setValue(0d);
            } else {
                double value = (partView.getValue() * 100) / percentView.getValue();
                totalView.setValue(value);
            }
        }


        // Find the part
        else {

            if (percentView.isEmpty() || totalView.isEmpty()) {
                partView.setValue(null);
            } else {
                double value = (percentView.getValue() * totalView.getValue()) / 100;
                partView.setValue(value);
            }

        }
    }
}

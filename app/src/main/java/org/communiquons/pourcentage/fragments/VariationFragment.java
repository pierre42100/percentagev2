package org.communiquons.pourcentage.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.communiquons.pourcentage.R;
import org.communiquons.pourcentage.views.ValueInputView;
import org.communiquons.pourcentage.views.ValueInputViewInterface;

public class VariationFragment extends Fragment implements ValueInputViewInterface.OnValueSelectedListener, ValueInputViewInterface.OnValueChangedListener {

    RadioButton increaseButton;
    RadioButton decreaseButton;

    ValueInputViewInterface initialValue;
    ValueInputViewInterface variationPercent;
    ValueInputViewInterface variationAmount;
    ValueInputViewInterface finalValue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_variation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        increaseButton = view.findViewById(R.id.increaseRadio);
        increaseButton.setOnCheckedChangeListener((v, i) -> refreshUI());

        decreaseButton = view.findViewById(R.id.decreaseRadio);
        decreaseButton.setOnCheckedChangeListener((v, i) -> refreshUI());

        initialValue = view.findViewById(R.id.initialValue);
        initialValue.setLabel(R.string.initalValue);
        initialValue.setOnValueSelectedListener(this);
        initialValue.setOnValueChangedListener(this);

        variationPercent = view.findViewById(R.id.variationPercent);
        variationPercent.setLabel(R.string.variationPercent);
        variationPercent.setOnValueSelectedListener(this);
        variationPercent.setOnValueChangedListener(this);

        variationAmount = view.findViewById(R.id.variationAmount);
        variationAmount.setLabel(R.string.variationAmount);
        variationAmount.setOnValueSelectedListener(this);
        variationAmount.setOnValueChangedListener(this);

        finalValue = view.findViewById(R.id.finalValue);
        finalValue.setLabel(R.string.finalValue);
        finalValue.setSelected(true);
        finalValue.setOnValueSelectedListener(this);
        finalValue.setOnValueChangedListener(this);

        refreshUI();
    }

    @Override
    public void onSelected(ValueInputView v) {

        increaseButton.setEnabled(v != variationPercent && v != variationAmount);
        decreaseButton.setEnabled(v != variationPercent && v != variationAmount);

        initialValue.setSelected(v == initialValue);
        variationPercent.setSelected(v == variationPercent || v == variationAmount);
        variationAmount.setSelected(v == variationPercent || v == variationAmount);
        finalValue.setSelected(v == finalValue);
    }

    @Override
    public void onChanged(double value, ValueInputView v) {
        if (v == variationPercent)
            variationAmount.setValueWithoutCb(null);

        if (v == variationAmount)
            variationPercent.setValueWithoutCb(null);

        refreshUI();


    }

    private double variation() {
        return increaseButton.isChecked() ? 1f : -1f;
    }

    private void refreshUI() {

        // Find final value
        if (finalValue.isSelected()) {
            if (initialValue.isEmpty()
                    || (variationPercent.isEmpty() && variationAmount.isEmpty())) {
                finalValue.setValue(null);
            }

            // %
            else if (!variationPercent.isEmpty()) {
                double finVal = initialValue.getValue()
                        + variation() * (initialValue.getValue() * variationPercent.getValue()) / 100;
                finalValue.setValue(finVal);

                recomputeVariationAmount();
            }

            // amount
            else {
                double finVal = initialValue.getValue() + variation() * variationAmount.getValue();
                finalValue.setValue(finVal);

                recomputeVariationPercent();
            }
        }

        // Find initial value
        else if (initialValue.isSelected()) {
            if (finalValue.isEmpty()
                    || (variationPercent.isEmpty() && variationAmount.isEmpty())) {
                initialValue.setValue(null);
            }

            // - 100%
            else if (variationPercent.getValue() == 100d && decreaseButton.isChecked()) {
                initialValue.setValue(0d);
                variationAmount.setValueWithoutCb(finalValue.getValue());
            }

            // %
            else if (!variationPercent.isEmpty()) {
                double value = finalValue.getValue() / ((variation() * variationPercent.getValue()) / 100 + 1);
                initialValue.setValue(value);

                recomputeVariationAmount();
            }

            // amount
            else {
                double value = finalValue.getValue() - variation() * variationAmount.getValue();
                initialValue.setValue(value);

                recomputeVariationPercent();
            }
        }

        // Else find variation
        else {

            if (initialValue.isEmpty() || finalValue.isEmpty()) {
                variationAmount.setValue(null);
                variationPercent.setValue(null);
            }

            double amount = finalValue.getValue() - initialValue.getValue();
            variationAmount.setValue(amount);

            recomputeVariationPercent();
        }

    }

    private void recomputeVariationAmount() {
        double value = initialValue.getValue() * variationPercent.getValue() / 100;
        variationAmount.setValueWithoutCb(value);
    }

    private void recomputeVariationPercent() {
        Double value = null;

        if (initialValue.getValue() > 0)
            value = 100 * variationAmount.getValue() / initialValue.getValue();

        variationPercent.setValueWithoutCb(value);
    }
}


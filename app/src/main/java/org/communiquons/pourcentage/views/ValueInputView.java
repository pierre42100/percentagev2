package org.communiquons.pourcentage.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import org.communiquons.pourcentage.R;

public class ValueInputView extends FrameLayout implements ValueInputViewInterface {

    private TextView labelView;
    private EditText valueInput;
    private RadioButton selectRadio;

    private OnValueSelectedListener onValueSelectedListener;
    private OnValueChangedListener onValueChangedListener;

    public ValueInputView(@NonNull Context context) {
        super(context);
        init();
    }

    public ValueInputView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ValueInputView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View v = View.inflate(getContext(), R.layout.view_input, null);
        addView(v);

        labelView = v.findViewById(R.id.label);
        valueInput = v.findViewById(R.id.valueInput);
        selectRadio = v.findViewById(R.id.checkRadio);

        valueInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (onValueChangedListener != null && valueInput.isEnabled())
                    onValueChangedListener.onChanged(getValue(), ValueInputView.this);
            }
        });

        selectRadio.setOnCheckedChangeListener((compoundButton, b) -> {
            if (onValueSelectedListener != null && b)
                onValueSelectedListener.onSelected(ValueInputView.this);
        });
    }

    public void setLabel(@StringRes int resId) {
        labelView.setText(resId);
    }

    public void setSelected(boolean selected) {
        setInputEnabled(!selected);
        selectRadio.setChecked(selected);
    }

    public boolean isSelected() {
        return selectRadio.isChecked();
    }

    public void setValue(@Nullable Double value) {
        String newValue;

        if (value == null)
            newValue = "";

        else
            newValue = String.valueOf(value);

        if (!newValue.equals(valueInput.getText().toString()))
            valueInput.setText(newValue);
    }

    public void setValueWithoutCb(@Nullable Double value) {
        boolean enabled = valueInput.isEnabled();
        setInputEnabled(false);
        setValue(value);
        setInputEnabled(enabled);
    }

    public double getValue() {
        String value = valueInput.getText().toString();
        return value.isEmpty() ? 0d : Double.parseDouble(value);
    }

    @Override
    public boolean isEmpty() {
        return valueInput.getText().length() == 0;
    }

    public void setOnValueSelectedListener(@NonNull OnValueSelectedListener onValueSelectedListener) {
        this.onValueSelectedListener = onValueSelectedListener;
    }

    public void setOnValueChangedListener(@NonNull OnValueChangedListener onValueChangedListener) {
        this.onValueChangedListener = onValueChangedListener;
    }

    @Override
    public void setInputEnabled(boolean enabled) {
        if (valueInput.isEnabled() != enabled)
            valueInput.setEnabled(enabled);
    }
}

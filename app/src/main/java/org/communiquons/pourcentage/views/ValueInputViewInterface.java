package org.communiquons.pourcentage.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

public interface ValueInputViewInterface {
    interface OnValueSelectedListener {
        void onSelected(ValueInputView v);
    }

    interface OnValueChangedListener {
        void onChanged(double value, ValueInputView v);
    }

    void setLabel(@StringRes int resId);

    void setSelected(boolean selected);

    boolean isSelected();

    void setValue(@Nullable Double value);

    void setValueWithoutCb(@Nullable Double value);

    double getValue();

    boolean isEmpty();

    void setOnValueSelectedListener(@NonNull ValueInputView.OnValueSelectedListener onValueSelectedListener);

    void setOnValueChangedListener(@NonNull ValueInputView.OnValueChangedListener onValueChangedListener);

    void setInputEnabled(boolean blocked);
}

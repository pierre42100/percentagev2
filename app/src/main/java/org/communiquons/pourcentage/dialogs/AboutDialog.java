package org.communiquons.pourcentage.dialogs;

import android.app.AlertDialog;
import android.content.Context;

import org.communiquons.pourcentage.R;

public class AboutDialog {

    public static void show(Context context) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.about_dialog_title)
                .setMessage(R.string.about_dialog_message)
                .setPositiveButton(R.string.about_dialog_ok, (dialogInterface, i) -> {})
                .show();
    }

}
